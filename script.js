initialData()

function initialData(){
    $.ajax({
        url: "https://www.themealdb.com/api/json/v1/1/categories.php",
        type: "GET",
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/x-www-form-urlencoded',
        },
        success: function(successData){
            console.log(successData)
            var dataList = successData.categories;
            var list = '';
            for (let i = 0; i < dataList.length; i++) {
                list += '<div class="col-3" onclick=detailList("'+dataList[i].strCategory+'")>';
                list += '<div class="p-3 border bg-light">';
                list += '<div class="card">';
                list += '<img src="'+dataList[i].strCategoryThumb+'" class="card-img-top" alt="'+dataList[i].strCategory+'">';
                list += '<div class="card-body">'+dataList[i].strCategory+'</div>';
                list += '</div>';
                list += '</div>';
                list += '</div>';
                list += '</div>';

                var breadcrumbList = '';
                breadcrumbList += '<li class="breadcrumb-item"><a onclick="initialData()">Home</a></li>';
            }
            document.getElementById("list").innerHTML = list;
            document.getElementById("breadcrumbList").innerHTML = breadcrumbList;
        }
    })
}

function detailList(name){

    $.ajax({
        url: "https://www.themealdb.com/api/json/v1/1/filter.php?c="+name,
        type: "GET",
        headers:{
        'Accept':'application/json',
        'Content-Type':'application/x-www-form-urlencoded',
    },
    success: function(successData){
        console.log(successData)
        var dataList = successData.meals;
        var list = '';
        for (let i = 0; i < dataList.length; i++) {
            list += '<div class="col-3" onclick=detailData("'+dataList[i].idMeal+'","'+ name+'")>';
            list += '<div class="p-3 border bg-light">';
            list += '<div class="card">';
            list += '<img src="'+dataList[i].strMealThumb+'" class="card-img-top" alt="'+dataList[i].strMeal+'">';
            list += '<div class="card-body">'+dataList[i].strMeal+'</div>';
            list += '</div>';
            list += '</div>';
            list += '</div>';
            list += '</div>';

            var breadcrumbList = '';
            breadcrumbList += '<li class="breadcrumb-item"><a onclick="initialData()">Home</a></li>';
            breadcrumbList += '<li class="breadcrumb-item active" aria-current="page"><a onclick=detailList("'+name+'")>'+name+'</a></li>'
        }
        document.getElementById("list").innerHTML = list;
        document.getElementById("breadcrumbList").innerHTML = breadcrumbList;
    }
    })
}

function detailData(id, mealName){
    $.ajax({
        url: "https://www.themealdb.com/api/json/v1/1/lookup.php?i="+id,
        type: "GET",
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/x-www-form-urlencoded',
        },
        success: function(successData){
            console.log(successData)
            var dataList = successData.meals;
            var list = '';
            for (let i = 0; i < dataList.length; i++) {
                list += '<div class="card mb-3">';
                list += '<div class="row g-0">';
                list += '<div class="col-md-4">';
                list += '<img src="'+dataList[i].strMealThumb+'" class="img-fluid rounded-start" alt="'+dataList[i].strMeal+'">';
                list += '</div>';
                list += '<div class="col-md-8">';
                list += '<div class="card-body">';
                list += '<h5 class="card-title">'+dataList[i].strMeal+'</h5>';
                list += '<h6>Instructions</h6>';
                list += '<p class="card-text">'+dataList[i].strInstructions+'</p>';
                list += '<h6>Recipes</h6>';
                list += '<li>'+dataList[i].strMeasure1+'</li>';
                list += '<li>'+dataList[i].strMeasure2+'</li>';
                list += '<li>'+dataList[i].strMeasure3+'</li>';
                list += '<li>'+dataList[i].strMeasure4+'</li>';
                list += '<li>'+dataList[i].strMeasure5+'</li>';
                list += '<li>'+dataList[i].strMeasure6+'</li>';
                list += '<li>'+dataList[i].strMeasure7+'</li>';
                list += '<li>'+dataList[i].strMeasure8+'</li>';
                list += '<li>'+dataList[i].strMeasure9+'</li>';
                list += '<li>'+dataList[i].strMeasure10+'</li>';
                list += '<li>'+dataList[i].strMeasure11+'</li>';
                list += '<li>'+dataList[i].strMeasure12+'</li>';
                list += '<li>'+dataList[i].strMeasure13+'</li>';
                list += '<li>'+dataList[i].strMeasure14+'</li>';
                list += '<li>'+dataList[i].strMeasure15+'</li>';
                list += '<li>'+dataList[i].strMeasure16+'</li>';
                list += '<li>'+dataList[i].strMeasure17+'</li>';
                list += '<li>'+dataList[i].strMeasure18+'</li>';
                list += '<li>'+dataList[i].strMeasure19+'</li>';
                list += '<li>'+dataList[i].strMeasure20+'</li>';
                list += '<hr>'
                list += '<iframe width="420" height="315" src="https://www.youtube.com/embed/'+dataList[i].strYoutube.substring(32)+'"></iframe>'
                list += '<p class="card-text"><small class="text-muted">'+dataList[i].strTags+'</small></p>'
                list += '</div>'
                list += '</div>'
                list += '</div>'
                list += '</div>';

                var breadcrumbList = '';
                breadcrumbList += '<li class="breadcrumb-item"><a onclick="initialData()">Home</a></li>';
                breadcrumbList += '<li class="breadcrumb-item active" aria-current="page"><a onclick=detailList("'+mealName+'")>'+mealName+'</a></li>'
                breadcrumbList += '<li class="breadcrumb-item active" aria-current="page"><a>'+dataList[i].strMeal+'</a></li>'
            }
            document.getElementById("list").innerHTML = list;
            document.getElementById("breadcrumbList").innerHTML = breadcrumbList;
        }
    })
}